const path = require('path')

module.exports = {
  mode: 'development',
  entry: './src/index.jsx',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/dist/'
  },
  serve: {
    devMiddleware: {
      publicPath: '/dist/'
    }
  },
  module: {
    rules: [{
      test: /\.jsx$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          'presets': ['env'],
          'plugins': [
            ['transform-react-jsx', {
              'pragma': 'm'
            }],
            'transform-runtime'
          ]
        }
      }
    }]
  },
  devtool: 'source-map'
}
