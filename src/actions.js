import {
  chunk as _chunk,
  times as _times
} from 'lodash'
import debugGen from 'debug'

import store from './store'

const debug = debugGen('index')

const FILLER_TEXT = ' '

function randNBackInt () {
  return Math.round((Math.random() * 9))
}

function fillArray (array, length) {
  const lengthDiff = length - array.length
  if (lengthDiff) {
    _times(lengthDiff, () => {
      array.push(FILLER_TEXT)
    })
  }
}

function checkAnswer (key, currentValue, nBackValue) {
  if (nBackValue !== null) {
    switch (key) {
      case 'y':
        return currentValue === nBackValue
      case 'n':
        return !(currentValue === nBackValue)
    }
  } else {
    return null
  }
}

const actions = {
  resetTime () {
    const timestamp = window.performance.now()
    store.previousTime = timestamp
  },
  completeTrial (key) {
    const timestamp = window.performance.now()
    const { previousTime, list, nColumns } = this.getStore()
    const listLength = list.length

    const timeTakenForTrial = timestamp - previousTime

    const currentListIndex = listLength - 1
    const numberOfItem = list[currentListIndex]
    const currentNBackIndex = currentListIndex - nColumns
    const currentNBackItem = currentNBackIndex >= 0
      ? list[currentNBackIndex]
      : null
    const correctAnswer = checkAnswer(key, numberOfItem, currentNBackItem)

    const trial = {
      itemValue: numberOfItem,
      timeTakenForTrial,
      nBackValue: currentNBackItem,
      correct: correctAnswer,
      keyPressed: key,
      timeOfTrial: Date.now()
    }

    store.trialData.push(trial)

    // Resetting the clock for the next trial.
    this.resetTime()

    // Adding a new item to the list
    this.addNewItemToList()
  },
  getStore () {
    return JSON.parse(JSON.stringify(store))
  },
  getTable () {
    const table = []
    const { list, nColumns, nRows } = this.getStore()
    // Making table an empty array of arrays of strings
    _times(nRows, () => {
      const tableRow = []
      _times(nColumns, () => {
        tableRow.push(FILLER_TEXT)
      })
      table.push(tableRow)
    })

    const chunkedListItems = _chunk(list, nColumns)

    // If the trial has ended
    if (chunkedListItems.length > nColumns) {
      return null
    }

    const currentRowItemsIndex = chunkedListItems.length - 1
    const currentRowListItems = chunkedListItems[currentRowItemsIndex]
    const currentItemInRowIndex = currentRowListItems.length - 1
    const currentItem = currentRowListItems[currentItemInRowIndex]
    const { firstRow } = this.getStore()

    // Make the current row array
    let currentRow

    if (firstRow) {
      currentRow = currentRowListItems
      fillArray(currentRow, nColumns)
    } else {
      currentRow = _times(nColumns, () => FILLER_TEXT)
      currentRow[currentItemInRowIndex] = currentItem
    }

    // Replace the array in the table
    table[currentRowItemsIndex] = currentRow

    return table
  },
  setWarning (bool) {
    store.warning = bool
  },
  getData () {
    return JSON.parse(JSON.stringify(store.trialData))
  },
  setDisplayedComponent (component) {
    store.displayedComponent = component
  },
  getDisplayedComponent: () => store.displayedComponent,
  addNewItemToList () {
    const { list, nColumns } = this.getStore()
    const listLength = list.length
    const nextListIndex = listLength
    const nextNBackValue = list[nextListIndex - nColumns]

    if (store.list.length < store.nColumns) {
      debug('Initial row, adding random item to table')
      store.list.push(randNBackInt())
    } else {
      if (Math.random() > 0.5) {
        debug('Adding nBackValue to table')
        store.list.push(nextNBackValue)
      } else {
        debug('Adding random item to table')
        store.list.push(randNBackInt())
      }
    }
  },
  initializeStore () {
    const { nColumns } = this.getStore()
    let initialValues = _times(nColumns, randNBackInt)
    store.list = initialValues
  },
  setFirstRow (bool) {
    store.firstRow = bool
  }
}

export default actions
