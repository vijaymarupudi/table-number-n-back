import m from 'mithril'
/** @jsx m */

import actions from './actions'
import screenfull from 'screenfull'

function sleep (timeout) {
  return new Promise((resolve, reject) => {
    setTimeout(resolve, timeout)
  })
}

function sendDataToParent () {
  const dataToSend = JSON.stringify(actions.getStore())
  window.opener.postMessage(dataToSend, '*')
  setTimeout(() => { window.close() }, 1000)
}

function nBackKeypressHandler (event) {
  if (['y', 'n'].includes(event.key)) {
    actions.completeTrial(event.key)
    // If there is a table
    if (actions.getTable()) {
      m.redraw()
    } else {
      // Experiment is over
      if (window.opener) sendDataToParent()
      screenfull.exit()
      actions.setDisplayedComponent(RedirectionMessage)
      m.redraw()
    }
  } else {
    // Wrong key
    actions.setWarning(true)
    m.redraw()
  }
}

async function startExperiment (event) {
  actions.initializeStore()
  const { initialDisplayDuration } = actions.getStore()
  screenfull.request()
  // Show a loading screen first so that the fullscreen popup goes away
  actions.setDisplayedComponent(Loading)
  await sleep(4000)
  // Allow participant to see the items first
  actions.setDisplayedComponent(NBack)
  m.redraw()
  await sleep(initialDisplayDuration)
  actions.addNewItemToList()
  actions.setFirstRow(false)
  document.addEventListener('keypress', nBackKeypressHandler)
  m.redraw()
}

const TableRow = {
  view ({ attrs: { row } }) {
    return (
      <tr>
        {/* Adding the ' ' (space) check to allow for proper column heights. */}
        {row.map((item, idx) => <td key={idx}>{item === ' ' ? m.trust('&nbsp;') : item}</td>)}
      </tr>
    )
  }
}

const Table = {
  view ({ attrs: { table } }) {
    return (
      <table class='table nback-table'>
        <tbody>
          {table.map((row, idx) => <TableRow key={idx} row={row} />)}
        </tbody>
      </table>
    )
  }
}

const NBack = {
  oninit () {
    actions.resetTime()
  },
  view ({ attrs: { table } }) {
    const store = actions.getStore()
    const warningStatus = store.warning
    // Turn off warning, it will be displayed this time.
    actions.setWarning(false)
    return (
      <div>
        <Table table={table} />
        {
          warningStatus
            ? <div class='notification is-danger'>Please type <code>y</code> if the element above was the same, and <code>n</code> if it wasn't.</div>
            : null
        }
      </div>
    )
  }
}

const Instructions = {
  view () {
    return (
      <div className='hero is-fullheight'>
        <div className='hero-body'>
          <div className='container'>
            <h1 className='title'>Instructions</h1>
            <div className='content'>
              <p>These are sample instructions for the NBack.</p>
              <p>Press y if the item above was the same, and n if it was not.</p>
              <p><button class='button' onclick={startExperiment}>Click here to continue</button></p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const Message = {
  view: (vnode) => (
    <div className='hero is-fullheight'>
      <div className='hero-body'>
        <div className='container'>
          <h1 className='title'>{vnode.children}</h1>
        </div>
      </div>
    </div>
  )
}

const RedirectionMessage = {
  view () {
    return <Message>Please wait while you are redirected.</Message>
  }
}

const Loading = {
  view () {
    return <Message>Loading...</Message>
  }
}

const Display = {
  view (vnode) {
    const Component = actions.getDisplayedComponent()
    if (Component === NBack) {
      return <NBack table={actions.getTable()} />
    } else if (Component) {
      return <Component />
    } else {
      return <Instructions />
    }
  }
}

function main () {
  m.mount(document.querySelector('#app'), Display)
}

main()
