const store = {
  // displayedComponent initial stage
  displayedComponent: null,
  // Initial state of the list
  previousTime: null,
  list: [Math.round((Math.random() * 9))],
  trialData: [],
  // Number of columns and rows
  nColumns: 5,
  nRows: 5,
  initialDisplayDuration: 2000,
  // booleans
  warning: false,
  firstRow: true
}

// For browser debugging purposes
window.store = store

export default store
