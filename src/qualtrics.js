/* globals Qualtrics */

Qualtrics.SurveyEngine.addOnload(function () {
  /* Place your JavaScript here to run when the page loads */
  var qual = this
  // Prevent next button when the page loads
  qual.disableNextButton()

  // Listen for message
  window.addEventListener('message', (e) => {
    console.log(this)
    Qualtrics.SurveyEngine.setEmbeddedData('nBackData', e.data)
    qual.enableNextButton()
    qual.clickNextButton()
  })
})
